#!/bin/sh

#dependencia para pillow
sudo apt-get build-dep python-imaging -y
sudo ln -sf /usr/lib/`uname -i`-linux-gnu/libfreetype.so /usr/lib/
sudo ln -sf /usr/lib/`uname -i`-linux-gnu/libjpeg.so /usr/lib/
sudo ln -sf /usr/lib/`uname -i`-linux-gnu/libz.so /usr/lib/

# Configuramos GIT
sudo apt-get install git
git config --global user.name "Bienvenido Sáez Muelas"
git config --global user.email "bienvenidosaez@gmail.com"

# Instalamos npm, nodejs y gulp para los estáticos
sudo apt-get update
sudo apt-get install -y build-essential libssl-dev
sudo apt-get install -y nodejs
sudo apt-get install -y npm
sudo npm install -g gulp

node -v
npm -v

#Redis
apt-get install -y redis-server
